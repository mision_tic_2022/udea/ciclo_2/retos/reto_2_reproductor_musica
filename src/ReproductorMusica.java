
public class ReproductorMusica{
    //ATRIBUTOS
    String[] canciones;
    int[] cancionesFavoritas;
    boolean pausado;
    int cancionReproduciendo;

    public ReproductorMusica(String[] canciones){
        this.canciones = canciones;
        pausado = true;
        cancionReproduciendo = 0;
        cancionesFavoritas = new int[ canciones.length ];
        for(int i = 0; i < cancionesFavoritas.length; i++){
            cancionesFavoritas[i] = -1;
        }
    }

    //CONSULTORES

    public String[] getCanciones() {
        return canciones;
    }

    public int[] getCancionesFavoritas() {
        return cancionesFavoritas;
    }

    public boolean isPausado() {
        return pausado;
    }

    public int getCancionReproduciendo() {
        return cancionReproduciendo;
    }

    public void setCanciones(String[] canciones) {
        this.canciones = canciones;
    }

    //MODIFICADORES

    public void setCancionesFavoritas(int[] cancionesFavoritas) {
        this.cancionesFavoritas = cancionesFavoritas;
    }

    public void setPausado(boolean pausado) {
        this.pausado = pausado;
    }

    public void setCancionReproduciendo(int cancionReproduciendo) {
        this.cancionReproduciendo = cancionReproduciendo;
    }

    //ACCIONES
    public void proximaCancion(){
        cancionReproduciendo = (cancionReproduciendo +1)%canciones.length;
    }

    public void devolverCancion(){
        cancionReproduciendo = (cancionReproduciendo + canciones.length -1)%canciones.length;
    }

    public void cambiarEstadoReproduccion(){
        pausado = !pausado;
    }

    public void agregarCancionesFavoritas(){
        
    }


}